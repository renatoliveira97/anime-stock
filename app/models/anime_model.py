from app.models import database_connection, commit_and_close
from psycopg2 import sql
from datetime import datetime


class Anime():

    FIELDNAMES: list = ["id", "anime", "released_date", "seasons"]


    def __init__(self, anime: str, released_date: datetime, seasons: int) -> None:
        self.anime: str = anime.title()
        self.released_date: datetime = released_date
        self.seasons: int = seasons


    @staticmethod
    def get_all() -> list:

        conn, cur = database_connection()

        cur.execute("""
            SELECT * FROM animes
        """)

        data = cur.fetchall()

        processed_data: list = [dict(zip(Anime.FIELDNAMES, row)) for row in data]

        commit_and_close(conn, cur)

        return processed_data

    
    @staticmethod
    def get_unique(id: int) -> list:

        conn, cur = database_connection()

        query = sql.SQL("""
            SELECT * FROM animes WHERE id = {anime_id}
        """).format(anime_id = sql.Literal(id))

        cur.execute(query)

        data = cur.fetchall()

        processed_data = [dict(zip(Anime.FIELDNAMES, row)) for row in data]

        commit_and_close(conn, cur)

        return processed_data


    def save_anime(self) -> list:

        conn, cur = database_connection()

        new_anime: list = list(self.__dict__.values())

        query = sql.SQL("""
            INSERT INTO 
                animes(anime, released_date, seasons)
            VALUES
                ({anime_data}) 
            RETURNING *;
        """).format(anime_data = sql.SQL(',').join(sql.Literal(value) for value in new_anime))

        cur.execute(query)

        data = cur.fetchall()

        processed_data = [dict(zip(Anime.FIELDNAMES, row)) for row in data]

        commit_and_close(conn, cur)

        return processed_data


    @staticmethod
    def update_anime(id: int, data: dict) -> dict:

        conn, cur = database_connection()

        if "anime" in data:
            data["anime"] = data["anime"].title()

        columns = [sql.Identifier(key) for key in data.keys()]
        values = [sql.Literal(value) for value in data.values()]

        query = sql.SQL("""
            UPDATE
                animes
            SET
                ({data_keys}) = ({data_values})
            WHERE
                id = {anime_id} RETURNING *
        """).format(data_keys = sql.SQL(",").join(columns),
                    data_values = sql.SQL(",").join(values),
                    anime_id = sql.Literal(id))

        cur.execute(query)

        processed_data = cur.fetchone()

        commit_and_close(conn, cur)

        return dict(zip(Anime.FIELDNAMES, processed_data))


    @staticmethod
    def delete_anime(id) -> bool:

        conn, cur = database_connection()

        query = sql.SQL("""
            SELECT * FROM animes WHERE id = {anime_id}
        """).format(anime_id = sql.Literal(id))

        cur.execute(query)

        data = cur.fetchall()

        if data == []:
            return False

        query = sql.SQL("""
            DELETE FROM animes WHERE id = {anime_id}
        """).format(anime_id = sql.Literal(id))

        cur.execute(query)

        commit_and_close(conn, cur)

        return True


    @staticmethod
    def check_if_exists(data: dict) -> bool:

        exists = False

        conn, cur = database_connection()

        if "anime" in data:
            query = sql.SQL("""
                SELECT * FROM animes WHERE anime = {name}
            """).format(name = sql.Literal(data["anime"].title()))

            cur.execute(query)

            collected_data = cur.fetchall()

            if collected_data != []:
                exists = True

        commit_and_close(conn, cur)

        return exists


    @staticmethod
    def check_keys(data: dict) -> dict:

        anime_keys = ["anime", "released_date", "seasons"]

        nonexistent_keys = []
        valid_keys = []
        missing_keys = []

        for key in data:
            if key not in anime_keys:
                nonexistent_keys.append(key)
            else:
                valid_keys.append(key)

        for key in anime_keys:
            if key not in data:
                missing_keys.append(key)

        return {
            "valid_keys": valid_keys,
            "invalid_keys": nonexistent_keys,
            "missing_keys": missing_keys       
        }




