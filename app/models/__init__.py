from environs import Env
import psycopg2


env: Env = Env()
env.read_env()

configuration: dict = {
    "host": env("DB_HOST"),
    "database": env("DB_NAME"),
    "user": env("DB_USERNAME"),
    "password": env("DB_PASSWORD")
}


def database_connection() -> tuple:
    conn = psycopg2.connect(**configuration)
    cur = conn.cursor()

    return conn, cur


def commit_and_close(conn, cur) -> None:
    conn.commit()
    cur.close()
    conn.close()


def create_table_if_not_exists() -> None:

    conn, cur = database_connection()

    query = """
        CREATE TABLE IF NOT EXISTS animes(
	        id BIGSERIAL PRIMARY KEY,
	        anime VARCHAR(100) NOT NULL UNIQUE,
	        released_date DATE NOT NULL,
	        seasons INTEGER NOT NULL
        );
    """

    cur.execute(query)

    commit_and_close(conn, cur)
    