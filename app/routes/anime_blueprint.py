from flask import Blueprint
from app.controllers.anime_controller import (
    get_create,
    filter,
    update,
    delete
)


bp: Blueprint = Blueprint("bp_animes", __name__, url_prefix="/animes")

bp.route("", methods=["GET", "POST"])(get_create)
bp.get("/<int:anime_id>")(filter)
bp.patch("/<int:anime_id>")(update)
bp.delete("/<int:anime_id>")(delete)
