from flask import jsonify, request
from app.models.anime_model import Anime
from app.models import create_table_if_not_exists


def get_create():

    if request.method == 'GET':
        data: list = Anime.get_all()

        return jsonify({
            "data": data
        }), 200

    else:
        try:
            data = request.get_json()

            if Anime.check_if_exists(data):
                return jsonify({
                    "message": "This anime already exists."
                }), 409

            anime: Anime = Anime(**data)

            create_table_if_not_exists()

            return jsonify({
                "data": anime.save_anime()
            }), 201
        
        except TypeError:
            return jsonify(
                Anime.check_keys(data)
            ), 422


def filter(anime_id: int):

    data: list = Anime.get_unique(anime_id)

    if data != []:
        return jsonify({
            "data": data
        }), 200
    else:
        return jsonify({
            "error": "Not Found"
        }), 404


def update(anime_id: int):

    data = request.get_json()

    check_keys_test = Anime.check_keys(data)

    if check_keys_test["invalid_keys"] == []:

        try:

            anime = Anime.update_anime(anime_id, data)

            return jsonify({
                "data": anime
            }), 200

        except TypeError:

            return jsonify({
                "error": "This anime doesn't exists."
            }), 404

    else:

        check_keys_test.pop("missing_keys")

        return jsonify(
            check_keys_test
        ), 422


def delete(anime_id):

    if Anime.delete_anime(anime_id):
        return "", 204
    else:
        return jsonify({
            "error": "This anime doesn't exists."
        })
